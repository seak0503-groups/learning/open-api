import React, { useEffect, useState } from "react";
import { Form } from "react-final-form";
import {
  Box,
  Button,
  Card,
  CardHeader,
  CardContent,
  Stack,
  CircularProgress,
} from "@mui/material";
import { TextField } from "mui-rff";

import usePostData from "src/hooks/usePostData";
import Dialog from "src/views/Dialog";

function App() {
  const [content, setContent] = useState(null);
  const [dialog, setDialog] = useState(false);
  const [query, setQuery] = useState(null);

  const { handlePostData, result } = usePostData({
    path: `/summaries`,
    initialData: null,
  });

  const onSubmit = async (values) => {
    setQuery(values.query);
    try {
      await handlePostData(values);
    } catch (error) {
      console.log("error発生", error);
      return error;
    }
  };

  const closeDialog = () => {
    setDialog(false);
  };

  console.log("resultは", result);

  useEffect(() => {
    if (result.data) {
      setContent(result.data.content);
    }
  }, [result]);

  useEffect(() => {
    if (content) {
      setDialog(true);
    }
  }, [content]);

  console.log("contentは", content);

  return (
    <>
      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit, submitting, form }) => (
          <Box
            component="form"
            noValidate
            autoComplete="off"
            onSubmit={handleSubmit}
          >
            <Card>
              <CardHeader title="要約を試す" />
              <CardContent sx={{ pt: 0 }}>
                <TextField
                  label="素材"
                  name="query"
                  required
                  multiline
                  rows={10}
                />
              </CardContent>
              <Stack
                direction={{
                  xs: "column",
                  sm: "row",
                }}
                spacing={1}
                flexWrap="wrap"
                sx={{ p: 3 }}
              >
                <Button
                  color="primary"
                  variant="outlined"
                  disabled={submitting}
                  size="medium"
                  onClick={form.reset}
                >
                  クリア
                </Button>
                <Box flexGrow={1} />
                <Button
                  color="primary"
                  disabled={submitting}
                  type="submit"
                  variant="contained"
                  size="medium"
                >
                  {submitting ? <CircularProgress size={24} /> : "要約する"}
                </Button>
              </Stack>
            </Card>
          </Box>
        )}
      />
      <Dialog
        query={query}
        content={content}
        open={dialog}
        onClose={closeDialog}
      />
    </>
  );
}

export default App;
