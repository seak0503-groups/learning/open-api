class V1::SummariesController < V1::Base
  def create
    query = params[:query]

    response = client.chat(
      parameters: {
        model: "gpt-3.5-turbo",
        messages: [
          {role: "user", content: "以下の文章を要約してください。"},
          {role: "user", content: query},
        ],
        temperature: 0.7,
      }
    )

    content = response.dig("choices", 0, "message", "content")

    render json: {content: content}
  end
end
