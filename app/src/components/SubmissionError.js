import { FORM_ERROR } from "final-form";

const SubmissionError = (err) => {
  const errorMessage = err.error.message
    ? err.error.message
    : "予期せぬエラーが発生しました。";

  console.error(err);

  return {
    ...err.error.fields,
    [FORM_ERROR]: errorMessage,
  };
};

export default SubmissionError;
