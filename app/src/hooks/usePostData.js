import { useState, useCallback } from "react";

import useMounted from "./useMounted";
import useApi from "./useApi";
import { webApiConfig } from "src/config";
import SubmissionError from "src/components/SubmissionError";

const usePostData = ({ path, initialData = {} }) => {
  const [loading, setLoading] = useState(false);
  const isMounted = useMounted();
  const apiInstance = useApi({ baseUri: webApiConfig.baseUri.bff });

  const [result, setResult] = useState({
    data: initialData,
    header: {},
  });

  const handlePostData = useCallback(
    async (patchData) => {
      setLoading(true);
      try {
        const response = await apiInstance.post(path, patchData);

        if (isMounted()) {
          setResult({
            data: response.data,
            header: response.headers,
          });
        }
      } catch (err) {
        throw SubmissionError(err);
      } finally {
        setLoading(false);
      }
    },
    [isMounted]
  );

  return { loading, handlePostData, result };
};

export default usePostData;
