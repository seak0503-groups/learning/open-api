class V1::Base < ApplicationController
  private

  def client
    @client ||= OpenAI::Client.new(access_token: ENV["CHATGPT_API_KEY"])
  end
end
