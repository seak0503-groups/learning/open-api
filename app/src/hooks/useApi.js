import axios from "axios";

const useApi = ({ baseUri }) => {
  const axiosInstance = axios.create({
    baseURL: baseUri,
  });

  axiosInstance.interceptors.response.use(
    (response) => response,
    (error) =>
      Promise.reject(
        (error.response && error.response.data) || "Something went wrong"
      )
  );

  return axiosInstance;
};

export default useApi;
