import React, { useState, useEffect } from "react";
import {
  Dialog,
  Box,
  Stack,
  Button,
  Typography,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
} from "@mui/material";

import usePostData from "src/hooks/usePostData";

const AuthGroupsSyncDialog = ({
  query,
  content = "データなし",
  onClose,
  open,
}) => {
  const [value, setValue] = useState(null);

  const { handlePostData, result, loading } = usePostData({
    path: `/lists`,
    initialData: null,
  });

  const onSubmit = async () => {
    try {
      await handlePostData({ query: query });
    } catch (error) {
      console.log("error発生", error);
      return error;
    }
  };

  useEffect(() => {
    setValue(content);
  }, [content]);

  useEffect(() => {
    if (result.data) {
      const text = result.data.content;
      const array = text.split("\n");
      const list = array.map((a) => (
        <List>
          <ListItem>
            <ListItemText primary={a} />
          </ListItem>
        </List>
      ));
      setValue(list);
    }
  }, [result]);

  return (
    <Dialog fullWidth maxWidth="md" onClose={onClose} open={open}>
      <Box sx={{ p: 3 }}>
        <Typography gutterBottom variant="h5">
          結果
        </Typography>
      </Box>
      <Stack spacing={2} sx={{ p: 3 }}>
        <Typography>{value}</Typography>
      </Stack>
      <Stack
        alignItems="center"
        direction="row"
        justifyContent="space-between"
        spacing={1}
        sx={{ p: 2 }}
      >
        <Button
          variant="outlined"
          color="warning"
          onClick={onClose}
          disabled={loading}
        >
          閉じる
        </Button>
        <Button
          variant="contained"
          color="warning"
          onClick={onSubmit}
          disabled={loading}
        >
          {loading ? (
            <CircularProgress size={24} color="warning" />
          ) : (
            "箇条書きリストを作成する"
          )}
        </Button>
      </Stack>
    </Dialog>
  );
};

export default AuthGroupsSyncDialog;
