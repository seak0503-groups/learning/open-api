export const webApiConfig = {
  baseUri: {
    bff: process.env.REACT_APP_BFF_URL,
  },
};
