#!/bin/sh

echo "Rails Install"

gem install rails --version="~>7.0.4"

echo "Create rails new app..."

rails new --skip-bundle --skip-action-mailer --skip-test --skip-action-cable --skip-action-mailbox --skip-action-text -O --api -T bff

rm -f ./bff/.ruby-version
